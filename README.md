# Focus

A really simple todo application with a front-end built in Ember, calling against a Rails API.

Both the front-end and back-end are included in this repo to make things easy. Normally, I'd prefer to have the two separate.

## Overview

- The project is built on [EmberCLI Rails](https://github.com/thoughtbot/ember-cli-rails).
- The Rails JSON API was made (arguable way too) easy with [JSONAPI::Resources](https://github.com/cerebris/jsonapi-resources)
- Acceptance tests on the front-end are mocked out with [EmberCLI Mirage](http://www.ember-cli-mirage.com).

## Requirements

- Ruby >= 2.2.2
- EmberCLI 1.13.13

## Getting Started

- `$ git clone https://ironkeith@bitbucket.org/ironkeith/focus.git`
- `$ bundle`
- `$ rake db:migrate`
- `$ rake ember:install`
- ☕️
- `$ rails s`
- Visit [http://localhost:3000](http://localhost:3000) in your browser (that first pageload will take a minute)

## Running Tests

To run the Rails test suite:

- `$ rails t`

To run the ember test suite:

- `$ rake ember:test`

To run the Ember test server:

- `$ cd frontend`
- `$ ember t -s`