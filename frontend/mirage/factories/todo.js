import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  text: faker.lorem.words,
  is_complete: false,
  created_at: faker.date.soon,
  sorted_at: faker.date.soon
});
