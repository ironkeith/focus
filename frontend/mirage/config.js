export default function() {
  this.passthrough();
}

export function testConfig() {
  this.namespace = 'api';
  this.resource('todos');
}
