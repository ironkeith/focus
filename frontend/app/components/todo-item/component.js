import Ember from 'ember';

const {
  Component,
  computed: { reads },
  get,
  RSVP: { resolve },
  run: { scheduleOnce },
  set
} = Ember;

export default Component.extend({
  tagName: 'li',

  /**
   * @type {Todo}
   * @required
   * @public
   */
  item: null,

  /**
   * Required method to save changes to the text
   * @type {function}
   * @param {string} text
   * @return {Promise}
   * @public
   */
  save(/* text */) {},

  isEditing: false,

  text: reads('item.text'),

  actions: {
    cancel() {
      set(this, 'isEditing', false);
      set(this, 'text', reads('item.text'));
    },

    edit() {
      set(this, 'isEditing', true);
      scheduleOnce('afterRender', this, () => this.$('input').focus());
    },

    save() {
      console.log('saving');
      let text = get(this, 'text');
      return resolve(this.save(text))
        .finally(() => set(this, 'isEditing', false));
    }
  }
});
