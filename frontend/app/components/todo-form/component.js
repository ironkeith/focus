import Ember from 'ember';
import { task } from 'ember-concurrency';

const {
  Component,
  computed,
  computed: {
    or,
    reads
  },
  get,
  set
} = Ember;

export default Component.extend({
  tagName: 'form',
  classNames: ['todo__form'],

  /**
   * @type {Todo}
   * @public
   * @required
   */
  item: null,

  /**
   * Pursist the new todo
   * @param {Todo} item
   * @param {Object} properties - values to be set on the Todo
   * @return {Promise<Todo>}
   * @public
   */
  save(/* item, properties */) {},

  value: reads('item.text'),

  disableSubmit: or('isInvalid', '_saveTask.isRunning'),

  isInvalid: computed('value', function() {
    return get(this, 'value').trim() === '';
  }),

  reset() {
    set(this, 'value', reads('item.text'));
  },

  didRender() {
    this.$('input').focus();
  },

  submit(e) {
    e.preventDefault();
    let value = get(this, 'value');
    return get(this, '_saveTask').perform(value);
  },

  _saveTask: task(function* (text) {
    yield this.save(get(this, 'item'), { text });
    this.reset();
  }).drop(),
});
