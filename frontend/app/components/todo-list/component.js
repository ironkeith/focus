import Ember from 'ember';

const {
  Component,
  computed: { sort }
} = Ember;

export default Component.extend({
  tagName: 'ul',
  classNames: ['todo__list'],

  /**
   * @type {Todo[]}
   * @public
   */
  items: null,

  /**
   * Required method to save changes to the text
   * @type {function}
   * @param {Todo} item
   * @param {string} text
   * @return {Promise<item>}
   * @public
   */
  save(/* item, text */) {},

  itemsSorting: ['sorted_at:desc', 'created_at:desc'],

  sortedItems: sort('items', 'itemsSorting'),

  actions: {
    save(item, text) {
      return this.save(item, { text });
    }
  }
});
