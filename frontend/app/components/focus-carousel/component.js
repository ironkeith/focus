import Ember from 'ember';
import { task } from 'ember-concurrency';

const {
  computed: {
    none
  },
  Component,
  isPresent,
  get,
  RSVP: { resolve },
  set
} = Ember;

export default Component.extend({
  classNames: ['carousel'],

  /**
   * @type {Todo[]}
   * @required
   * @public
   */
  items: null,

  /**
   * Required method that handles completing a todo
   * @required
   * @type {function}
   * @param {Todo} item
   * @return {Promise<Todo>}
   * @public
   */
  completeItem(/* item */) {},

  allDone: none('_current'),

  click(e) {
    e.preventDefault();
    if (get(this, 'allDone')) {
      return;
    }
    let task = get(this, '_completeTask');
    let item = get(this, '_current');
    task.perform(item).then(() => this.next());
  },

  init() {
    this._super(...arguments);
    this.next();
  },

  next() {
    let current = get(this, 'items')
      .filter((item) => isPresent(get(item, 'text')))
      .findBy('is_complete', false);
    set(this, '_current', current);
  },

  /**
   * @type {(Todo|undefined)}
   * @protected
   */
  _current: null,

  _completeTask: task(function* (item) {
    return yield this.completeItem(item);
  }).drop(),

  actions: {
    complete(item) {
      resolve(this.completeItem(item))
        .then(() => this.next())
    }
  }
});
