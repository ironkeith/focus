import Ember from 'ember';

const {
  computed: { readOnly },
  Controller,
  set
} = Ember;

export default Controller.extend({
  todos: readOnly('model'),

  actions: {
    completeItem(item) {
      set(item, 'is_complete', true);
      return item.save();
    }
  }
});
