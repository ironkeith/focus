import Ember from 'ember';

const {
  get,
  inject: { service },
  Route
} = Ember;

export default Route.extend({
  store: service(),

  model() {
    return get(this, 'store').findAll('todo');
  }
});
