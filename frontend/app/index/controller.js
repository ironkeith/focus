import Ember from 'ember';

const {
  computed: { filter },
  Controller,
  get,
  inject: { service },
  set,
  setProperties
} = Ember;

export default Controller.extend({
  store: service(),

  /**
   * @type {Todo}
   * @public
   */
  newTodo: null,

  /**
   * @type {Todo[]}
   * @public
   */
  todos: filter('model', function(item) {
    return !get(item, 'isNew');
  }),

  init() {
    this._super(...arguments);
    this.setupNewTodo();
  },

  setupNewTodo() {
    let newTodo = get(this, 'store').createRecord('todo');
    set(this, 'newTodo', newTodo);
  },

  actions: {
    createTodo(item, props) {
      setProperties(item, props);
      return item.save().then(() => this.setupNewTodo());
    },

    saveTodo(item, props) {
      if (get(props, 'text').trim() === '') {
        return item.destroyRecord();
      }
      setProperties(item, props);
      return item.save();
    }
  }
});
