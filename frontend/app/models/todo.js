import DS from 'ember-data';

const {
  attr,
  Model
} = DS;

export default Model.extend({
  text: attr('string', { defaultValue: '' }),
  is_complete: attr('boolean', { defaultValue: false }),
  created_at: attr('date'),
  sorted_at: attr('date')
});
