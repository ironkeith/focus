class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.string :text
      t.boolean :is_complete
      t.date :sorted_at

      t.timestamps
    end
  end
end
