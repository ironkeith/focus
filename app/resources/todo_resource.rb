class TodoResource < JSONAPI::Resource
end

module Api
  class TodoResource < JSONAPI::Resource
    attributes :text, :is_complete, :sorted_at, :created_at
  end
end