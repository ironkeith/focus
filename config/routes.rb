Rails.application.routes.draw do
  namespace :api do
    jsonapi_resources :todos
  end
  mount_ember_app :frontend, to: "/"
end
